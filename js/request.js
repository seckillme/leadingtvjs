
var requestConfig = {
    serverUrl:"http://www.sociaty1.com",//COLUMN201804231059553931
    index_title : "wenjuan",
    ca_key:"",
    request:{
        "questions":"/tv/question/questions",
        "options":"/tv/question/options",
        "answer":"/tv/question/submit",
        "votes":"/tv/vote/votes",
        "vote-detail":"/tv/vote/detail",
        "vote":"/tv/vote/voteAjax",

    }
};


function getRequestGlobalVar(_key) {
    if("undefined" != typeof(iPanel)) {
        return iPanel.getGlobalVar(_key) || "";
    }else if(typeof(localStorage) != 'undefined' && localStorage) {
    	return localStorage.getItem(_key);
    }else {
        return getCookie(_key);
    }
}

function setRequestGlobalVar(_key,_value) {
    if("undefined" != typeof(iPanel)) {
        iPanel.setGlobalVar(_key, _value + "");
    }else if(typeof(localStorage) != 'undefined'  && localStorage) {
    	localStorage.setItem(_key, _value);
    }else {
        setCookie(_key, _value + "");
    }
}


function jsonToUrlParam(param) {
    // "token":"4b0999c80812878caecff707ef7748a6",
    //     "time":"1",

    var urlEncode = "";

    for (var i in param){
        if(param.hasOwnProperty(i)){
            var Uri = encodeURIComponent(i) + "=" + encodeURIComponent(param[i]) +"&";
            urlEncode += Uri;
        }
    }

    return urlEncode;
}

function request(url,param,successCallback,errorCallback) {

    var data = jsonToUrlParam(param);
    var ajax_url = "";
    ajax_url = requestConfig.serverUrl+requestConfig.request[url];

    ajax({
        url: ajax_url+"?"+data,// 请求的URL,可传参数
        type: "GET", //HTTP 请求类型,GET或POST
        dataType: "html", //请求的文件类型html/xml
        onSuccess: function(re){ //请求成功后执行[可选]
            var json = eval('('+re+')');
            successCallback(json)
        },
        onError:function(){ //请求失败后执行[可选]
            var json = eval('('+re+')');
            errorCallback(json)
        },
        onComplete:function(){// 请求成功.失败或完成(无论成功失败都会调用)时执行的函数[可选]
            // alert("onComplete");
        },
        post:"",
        timeout:7000  //请求的超时时间，默认7000ms，也可自行设定[可选]
    });
}