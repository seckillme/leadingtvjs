

var requestConfig = {
    serverUrl:"http://114.115.238.201:8080/cms/",//10.215.0.19
    index_title : "金沙园",
    request:{
        "area":"/area/queryAreaCode",
        "column":"/column/queryColumn",
        "content":"/content/queryContent"
    }
};

//url参数工具
var Q = Query = {
    getFromURL: function(url,parameter){
        var index = url.indexOf("?");
        if (index != -1) {
            var parameterString = url.substr(index + 1);
            var reg = new RegExp("(^|&)" + parameter + "=([^&]*)(&|$)", "i");
            var r = parameterString.match(reg);
            if (r != null){
                return r[2];
            }
        }
        return null;
    },
    get: function(parameter) {
        if (typeof (parameter) == "undefined" || parameter == "") {
            return null;
        }
        var url = window.location.href;
        var index = url.indexOf("?");
        if (index != -1) {
            var parameterString = url.substr(index + 1);
            var reg = new RegExp("(^|&)" + parameter + "=([^&]*)(&|$)", "i");
            var r = parameterString.match(reg);
            if (r != null){
                return r[2];
            }
        }
        return null;
    },
    getInt:function(parameter,defaultValue){
        var value = parseInt(this.get(parameter));
        return isNaN(value) ? (typeof(defaultValue) == "undefined" ? 0 : defaultValue) : value;
    },
    getDecoded:function(parameter){
        return this.decode(this.get(parameter));
    },
    decode:function(srcStr){
        if(typeof(srcStr) == "undefined"){return null;}
        return decodeURIComponent(srcStr);
    },
    encode:function(srcStr){
        if(typeof(srcStr) == "undefined"){return null;}
        return encodeURIComponent(srcStr);
    },
    getSymbol:function(url){
        return url.indexOf("?") == -1 ? "?" : "&";
    },
    joinURL:function(url,queryString){
        return url + this.getSymbol(url) + queryString;
    },
    createQueryString:function(obj){
        var a = [];
        for(var p in obj){
            if(typeof(obj[p]) == "function" || obj[p] == null || typeof(obj[p])=="undefined") continue;
            a.push(p + "=" + obj[p]);
        }
        return a.join("&");
    }
};
// function getGlobalVar(_key) {
//     if("undefined" != typeof(iPanel)) {
//         return iPanel.getGlobalVar(_key) || "";
//     } else {
//         return getCookie(_key);
//     }
// }
function jsonToUrlParam(param) {
    // "token":"4b0999c80812878caecff707ef7748a6",
    //     "time":"1",

    var urlEncode = "";

    for (var i in param){
        if(param.hasOwnProperty(i)){
            var Uri = encodeURIComponent(i) + "=" + encodeURIComponent(param[i]) +"&";
            urlEncode += Uri;
        }
    }
    return urlEncode;
}

function request(url,param,successCallback) {

    // param.token = "4b0999c80812878caecff707ef7748a6";
    // param.time = 1;
    // param.ca = Q.getDecoded("ca");
    // if(param.ca == null || param.ca == "null" || param.ca == "undefined"){
    //     param.ca = getGlobalVar("ca");
    // }else{
    //     //从Url中获取到了ca则直接存入缓存中
    //     setGlobalVar("ca",param.ca);
    // }
    var data = jsonToUrlParam(param);

    ajax({
        url: requestConfig.serverUrl + requestConfig.request[url]+"?"+data,// 请求的URL,可传参数
        type: "GET", //HTTP 请求类型,GET或POST
        dataType: "html", //请求的文件类型html/xml
        onSuccess: function(re){ //请求成功后执行[可选]
            var json = eval('('+re+')');
            successCallback(json)
        },
        onError:function(){ //请求失败后执行[可选]

        },
        onComplete:function(){// 请求成功.失败或完成(无论成功失败都会调用)时执行的函数[可选]
            // alert("onComplete");
        },
        post:"",
        timeout:7000  //请求的超时时间，默认7000ms，也可自行设定[可选]
    });
}
