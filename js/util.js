
var Util = {
    /**
         * 截取文字加...
     * @param content
     * @param len
     * @return {*}
     */
    sliceWord: function sliceWord(content, len) {
        var templateWord = '';
        /* 自定义文字内容长度 */
        if (content.length * 2 <= len) {
            return content;
        }
        /* 用于记录文字内容的总长度 */
        var strLength = 0;
        for (var i = 0; i < content.length; i++) {
            templateWord = templateWord + content.charAt(i);
            /* charCodeAt()返回指定位置的字符的Unicode编码，值为128以下时一个字符占一位，当值在128以上是一个字符占两位 */
            if (content.charCodeAt(i) > 128) {
                strLength = strLength + 2;
                if (strLength >= len) {
                    return templateWord.substring(0, templateWord.length - 1) + "...";
                }
            } else {
                strLength = strLength + 1;
                if (strLength >= len) {
                    return templateWord.substring(0, templateWord.length - 2) + "...";
                }
            }
        }
        return templateWord;
    },
    ajax: function (_optionsObj, _cfFlag, _time_out) {
        var time_out = _time_out || new Date().getTime();
        var optionsObj = {
            // HTTP 请求类型
            type:        _optionsObj.type || "GET",
            // 请求的文件类型
            dataType:    _optionsObj.dataType,
            // 请求的URL
            url:        _optionsObj.url || "",
            //请求方式，true异步请求，false同步请求
            requestType: _optionsObj.requestType === false ? false:true,
            // 请求的超时时间
            time_out:    _optionsObj.time_out || 10000,
            // 请求成功.失败或完成(无论成功失败都会调用)时执行的函数
            onComplete: _optionsObj.onComplete || function(){},
            onError:    _optionsObj.onError || function(){},
            onSuccess:    _optionsObj.onSuccess || function(){},
            // 服务器端默认返回的数据类型
            data:        _optionsObj.data || "",
            post:        _optionsObj.post || ""
        };
        var cfFlag = _cfFlag || true;

        var ajaxZXFlag = true;
        // 强制关闭函数
        var timeOutRD = setTimeout(function(){
            clearTimeout(timeOutRD);
            ajaxZXFlag = false;
            if(!cfFlag) {
                ajax(optionsObj, true, time_out);
            } else {
                optionsObj.onError();

                //var time_in = new Date().getTime();
                //var time_c = time_in - time_out;
                //$("msgvalue").innerHTML = "time : " + time_c + " timeOutRD readyState : " + xml.readyState + " and xml.status : " + xml.status;
            }
        }, optionsObj.time_out);

        var xml = createXHR();
        xml.onreadystatechange = function() {
            if(xml.readyState == 4 && ajaxZXFlag){
                // 检查是否请求成功
                clearTimeout(timeOutRD);
                if(httpSuccess(xml) && ajaxZXFlag){
                    // 以服务器返回的数据作为参数执行成功回调函数
                    optionsObj.onSuccess(httpData(xml, optionsObj.dataType ));
                }else{
                    optionsObj.onError();
                    //var time_in = new Date().getTime();
                    //var time_c = time_in - time_out;
                    //$("msgvalue").innerHTML = "time : " + time_c + " timeOutRD readyState : " + xml.readyState + " and xml.status : " + xml.status;
                }

                // 调用完成后的回调函数
                optionsObj.onComplete(xml);
                // 避免内存泄露,清理文档
                xml = null;
            }
        }
        var url;
        if(optionsObj.url.indexOf("?") > -1) {
            url = optionsObj.url + "&timestamp=" + new Date().getTime();
        } else {
            url = optionsObj.url + "?timestamp=" + new Date().getTime();
        }
        xml.open(optionsObj.type, url, optionsObj.requestType);
        if("GET" == optionsObj.type){
            xml.send(null);
        }else{
            xml.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xml.send(optionsObj.post);
        }
    },
    timeAndWeather:timeAndWeather,
    navCheck:navCheck,
    showTitleLikeMarquee:showTitleLikeMarquee,
    showTitleForMarquee:function showTitleForMarquee(_title, _obj, _num) {
        if(_title.length > _num) {
            _obj.innerHTML = "<marquee>" + _title + "</marquee>";
        } else {
            _obj.innerHTML = _title;
        }
    },
    showTimeFn:showTimeFn,
    showTimeAndWether:timeAndWeather,
    setMarquee:function(title,left,top,color,font_size,icon){

        // var html =　"<div style='position: absolute;left: "+left+"px;top: "+top+"px;color: "+color+";font-size: "+font_size+";'>" +
        //     +"<img  src='"+icon+"' style='position: absolute;left: 10px;top: 10px;display:"+icon?"none":"inline-block"+"'; />"+
        //     +"<marquee style='position: absolute;left: 50px;top: 10px;'>"+title+"</marquee>"+
        //     +"</div>";
        // document.body.appendChild()
    },
    setLogo:function (src,left,top,container,style) {


        if(typeof container == "undefined"){
            var container = document.body;
        }
        var logo = document.createElement("img");
        logo.src = src;
        logo.style.position = "absolute";
        logo.style.left = left + "px";
        logo.style.top = top + "px";
        logo.style.zIndex = 1;
        if(typeof style != "undefined"){
            for(var i in style){
                if(style.hasOwnProperty(i)){
                    logo.style[i] = style[i];
                }
            }
        }

        container.insertBefore(logo,null);
    },
    setTitle:function (title,left,top,fontSize,container,style) {


        if(typeof container == "undefined"){
            container = document.body;
        }
        console.log(container);
        var logo = document.createElement("div");
        logo.innerHTML = title;
        logo.style.position = "absolute";
        logo.style.left = left + "px";
        logo.style.top = top + "px";
        logo.style.zIndex = 1;
        logo.style.fontSize = fontSize+"px";
        if(typeof style != "undefined"){
            for(var i in style){
                if(style.hasOwnProperty(i)){
                    logo.style[i] = style[i];
                }
            }
        }

        container.insertBefore(logo,null);
    }
};
var time_out;
function getCAcardNum() {
    var ca = "test123455";
    return ca;
}
function showTitleLikeMarquee(_title, _obj, _num, _time) {

    if(_title.length > _num) {
        var time = _time || 2500;

        var tempText = _title;
        var size=Math.ceil(tempText.length/_num);
        var array = [];
        for(var i=0;i<size;i++){
            var start=i*_num;
            var end=(i+1)*_num;
            if(end>tempText.length){
                end=tempText.length;
            }
            array.push(tempText.substring(start,end));
        }
        var temp=1;
        _obj.innerHTML=array[0];
        if(array.length>1){
            clearInterval(time_out);
            time_out = setInterval(function(){
                _obj.innerHTML=array[temp];
                if(temp>=(array.length-1)){
                    temp=0;
                }else{
                    temp++;
                }
            },time);
        }
    } else {
        _obj.innerHTML = _title;
    }
    return time_out;
}
function timeAndWeather(left,top) {

    var timeHtml = "<div style=\"position: absolute; left: "+left+"px; top:"+top+"px; width: 335px; height: 82px; font-size: 25px; color:#1b1b72;\">"+
        "    	<div style=\"position: absolute; left: 41px; top: 3px; width: 66px; height: 72px;\">"+
        "  			<img id=\"weather_img\" width=\"54\" height=\"40\" src=\"images/tm.gif\" />"+
        "    		<div id=\"temperature\" style=\"position: absolute; left: 0px; top: 45px; width: 64px; height: 23px; font-size: 15px; text-align: center;\"></div>"+
        " 	 </div>"+
        "		<div style=\"position: absolute; left: 111px; top: 12px; width: 65px; height: 30px; font-size: 25px;\">"+
        "        	<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-size:16px; line-height:30px; font-family:\'黑体\';overflow:hidden;\">"+
        "              <tr>"+
        "                <td align=\"right\"><span style=\"font-size:16px; padding-left:10px;\" id=\"year\"></span></td>"+
        "              </tr>"+
        "              <tr>  "+
        "    			<td width=\"190\" align=\"right\" style=\"font-size:16px; line-height:25px\"><span style=\"font-size:16px; padding-left:10px;\" id=\"week\"></span></td>"+
        "  			 </tr>"+
        "            </table>"+
        " 		</div>"+
        "	<div style=\"position: absolute; left: 185px; top: 24px; width: 1px; height: 38px; background-color:#1b1b72;\"></div>"+
        "		<div style=\"position: absolute; left: 185px; top: 1px; width: 124px; height: 80px; text-align: right;\">"+
        "			<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-size:20px; font-family:\'黑体\';\">"+
        " 			  <tr>   "+
        "   			 	<td width=\"150\" height=\"55\" align=\"right\" style=\" line-height:80px;\"><span style=\"font-size:36px; padding-right:16px;\" id=\"nowTime\"></span></td>"+
        " 			  </tr>"+
        "			</table>"+
        "		</div>"+
        "	</div>";
    var dom = document.createElement("div");
    dom.id = "timerContainer";
    dom.style = "position:absolute; left:0px; top:0px; width:1280px; height:720px;";
    document.getElementsByTagName("body")[0].appendChild(dom);
    dom.innerHTML = timeHtml;
    var nowTimeSetTimeout = -1;					//右下角当前时间计时器
    function showNowTime(){
        var d = new Date();
        var hour = d.getHours() + "";
        var minute = d.getMinutes() + "";
        var year = d.getFullYear() + "";
        var month = d.getMonth() + 1;
        var day = d.getDate() + "";
        var week;
        if(d.getDay()==0)          week="星期日"
        if(d.getDay()==1)          week="星期一"
        if(d.getDay()==2)          week="星期二"
        if(d.getDay()==3)          week="星期三"
        if(d.getDay()==4)          week="星期四"
        if(d.getDay()==5)          week="星期五"
        if(d.getDay()==6)          week="星期六"
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        year = year.length > 1 ? year : "0" + year;
        month = month > 9 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        document.getElementById("nowTime").innerHTML =  hour + ":" + minute;
        document.getElementById("year").innerHTML =  month + "/" + day;
        document.getElementById("week").innerHTML = week;
        nowTimeSetTimeout = setTimeout("showNowTime()",60000);
    }


    function showWeather(){
        var operator = getOperator();

        if(operator == ""){
            return;
        }

        Util.ajax({
            url: "http://10.215.0.36/weather/sy/"+operator+".js",//queryTitleByIdMobile.shtml?id=" + _id + "&userid=" + caNum,// 请求的URL,可传参数
            type: "GET", //HTTP 请求类型,GET或POST
            dataType: "html", //请求的文件类型html/xml
            onSuccess: function(html){ //请求成功后执行[可选]

                eval(html);
                var info = mainArray;
                var city = iPanel.misc.getUserCharsetStr(info[0].cityName,"UTF8");
                var weather = iPanel.misc.getUserCharsetStr(info[0].t0[0].weather,"UTF8");
                var temperature = iPanel.misc.getUserCharsetStr(info[0].t0[0].temperature,"UTF8");
                if(typeof city != "undefined" && city != "undefined" && city != ""){
                    //document.getElementById("city").innerText = city;
                }
                if(typeof temperature != "undefined" && temperature != "undefined" && temperature != ""){
                    document.getElementById("temperature").innerText = temperature;
                }
                if(typeof weather != "undefined" && weather != "undefined" && weather != ""){
                    //document.getElementById("weather").innerText = weather;
                    changeWeather(weather);
                }

            },
            onError:function(){ //请求失败后执行[可选]
                document.getElementById("temperature").innerText = "暂无天气信息";
            },
            onComplete:function(){// 请求成功.失败或完成(无论成功失败都会调用)时执行的函数[可选]
                // //alert("onComplete");
            },
            post:"",
            timeout:7000  //请求的超时时间，默认7000ms，也可自行设定[可选]
        });
    }
    var tempWeather=null;
    function changeWeather(_weather){
        switch(_weather){
            case "阵雨":
                tempWeather="images/weather/zhenyu.png";
                break;
            case "暴雪":
                tempWeather="images/weather/baoxue.png";
                break;
            case "暴雨":
                tempWeather="images/weather/baoyu.png";
                break;
            case "暴雨转大暴雨":
                tempWeather="images/weather/baoyuzhuandabaoyu.png";
                break;
            case "大暴雨":
                tempWeather="images/weather/dabaoyu.png";
            case "大暴雨转特大暴雨":
                tempWeather="images/weather/dabaoyuzhuantedabaoyu.png";
                break;
            case "大雾":
                tempWeather="images/weather/dawu.png";
                break;
            case "大雪":
                tempWeather="images/weather/daxue.png";
                break;
            case "大雪转暴雪":
                tempWeather="images/weather/daxuezhuanbaoxue.png";
                break;
            case "大雨":
                tempWeather="images/weather/dayu.png";
                break;
            case "大雨转暴雨":
                tempWeather="images/weather/dayuzhuanbaoyu.png";
                break;
            case "冬雨":
                tempWeather="images/weather/dongyu.png";
                break;
            case "多云":
                tempWeather="images/weather/duoyun.png";
                break;
            case "浮尘":
                tempWeather="images/weather/fuchen.png";
                break;
            case "雷阵雨":
                tempWeather="images/weather/leizhenyu.png";
                break;
            case "雷阵雨伴有冰雹":
                tempWeather="images/weather/leizhenyubanyoubingbao.png";
                break;
            case "雷阵雨冰雹":
                tempWeather="images/weather/leizhenyubingbao.png";
                break;
            case "强沙尘暴":
                tempWeather="images/weather/qiangshachenbao.png";
                break;
            case "晴":
                tempWeather="images/weather/qing.png";
                break;
            case "沙尘暴":
                tempWeather="images/weather/shachenbao.png";
                break;
            case "特大暴雨":
                tempWeather="images/weather/tedabaoyu.png";
                break;
            case "小雪":
                tempWeather="images/weather/xiaoxue.png";
                break;
            case "雾":
                tempWeather="images/weather/wu.png";
                break;
            case "小雪转中雪":
                tempWeather="images/weather/xiaoxuezhuanzhongxue.png";
                break;
            case "小雨":
                tempWeather="images/weather/xiaoyu.png";
                break;
            case "小雨转中雨":
                tempWeather="images/weather/xiaoyuzhuanzhongyu.png";
                break;
            case "扬沙":
                tempWeather="images/weather/yangsha.png";
                break;
            case "阴":
                tempWeather="images/weather/yin.png";
                break;
            case "雨夹雪":
                tempWeather="images/weather/yujiaxue.png";
                break;
            case "阵雪":
                tempWeather="images/weather/zhenxue.png";
                break;
            case "中雪":
                tempWeather="images/weather/zhongxue.png";
                break;
            case "中雪转大雪":
                tempWeather="images/weather/zhongxuezhuandaxue.png";
                break;
            case "中雨":
                tempWeather="images/weather/zhongyu.png";
                break;
            case "中雨转大雨":
                tempWeather="images/weather/zhongyuzhuandayu.png";
                break;
        }
        //document.getElementById("weather").innerText =tempWeather;
        document.getElementById("weather_img").src  = tempWeather;
    }

    /*获取operator区域*/
    function getOperator(){
        if(typeof DataAccess == "undefined"){
            return false;
        }
        var operator = "";
        var sysTable = DataAccess.getSystemPropertyTable();
        if(sysTable > 0){
            var data = DataAccess.getProperty(sysTable, "operator");
            Utility.println("portal getOperator data="+data);
            if(data != null){
                eval('var tmp=' + data);
                operatorKey = tmp.key;
                Utility.println("portal getOperator operatorKey="+operatorKey);
                switch(operatorKey){
                    case 0:
                    case "0":
                        operator = "";
                        break;
                    case 1:
                    case "1":
                        operator = "FUZHOU";
                        break;
                    case 2:
                    case "2":
                        operator = "NANPING";
                        break;
                    case 3:
                    case "3":
                        operator = "NINGDE";
                        break;
                    case 4:
                    case "4":
                        operator = "PUTIAN";
                        break;
                    case 5:
                    case "5":
                        operator = "SANMING";
                        break;
                    case 6:
                    case "6":
                        operator = "ZHANGZHOU";
                        break;
                    case 7:
                    case "7":
                        operator = "LONGYAN";
                        break;
                    case 8:
                    case "8":
                        operator = "QUANZHOU";
                        break;
                    case 9:
                    case "9":
                        operator = "XIAMEN";
                        break;
                    case 10:
                    case "10":
                        operator = "FUZHOU";
                        break;
                    case 20:
                    case "20":
                        operator = "FUZHOU";
                        break;
                }
            }
        }
        return operator;
    }

    return {
        showNowTime:showNowTime,
        showWether:showWeather
    }
}
<!--时间控制器-->
function showTimeFn(left,top) {

    var timeHtml = '<div style="position: absolute;left:'+left+'px;top: '+top+'px;color:#242629;">\n' +
        '    <div id="js-time_time" style="font-size: 60px;line-height: 83px;">\n' +
        '        17:12' +
        '    </div>\n' +
        '    <div>\n' +
        '        <div style="font-size: 20px;margin-left: 5px;" id="js-time_date">\n' +
        '            08月28日\n' +
        '        </div>\n' +
        '        <div id="js-time_week" style="font-size: 20px;margin-left: 5px;">星期一</div>\n' +
        '    </div>\n' +
        '</div>';
    var dom = document.createElement("div");
    dom.id = "timerContainer";
    dom.style = "position:absolute; left:0px; top:0px; width:1280px; height:720px;";
    document.body.appendChild(dom);
    dom.innerHTML = timeHtml;
    var nowTimeSetTimeout = -1;					//右下角当前时间计时器
    function showNowTime(){
        var d = new Date();
        var hour = d.getHours() + "";
        var minute = d.getMinutes() + "";
        var year = d.getFullYear() + "";
        var month = d.getMonth() + 1;
        var day = d.getDate() + "";
        var week;
        if(d.getDay()==0)          week="星期日"
        if(d.getDay()==1)          week="星期一"
        if(d.getDay()==2)          week="星期二"
        if(d.getDay()==3)          week="星期三"
        if(d.getDay()==4)          week="星期四"
        if(d.getDay()==5)          week="星期五"
        if(d.getDay()==6)          week="星期六"
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        year = year.length > 1 ? year : "0" + year;
        month = month > 9 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        document.getElementById("js-time_time").innerHTML =  hour + ":" + minute;
        document.getElementById("js-time_date").innerHTML =  month + "/" + day;
        document.getElementById("js-time_week").innerHTML = week;
        nowTimeSetTimeout = setTimeout(showNowTime,60000);
    }
    return {
        showNowTime:showNowTime,
    }
}



function PlayVideo() {
    this.ip = typeof  iPanel != "undefined"?iPanel.getGlobalVar("ip"):"";// ip
    this.port = typeof  iPanel != "undefined"?iPanel.getGlobalVar("port"):"";//端口
    this.account = typeof  GlobalVarManager != "undefined"? GlobalVarManager.getItemStr("account"):"";//密匙？
}

PlayVideo.prototype = {
    smallPlayUrl:"",
    mediaID:0,
    mediaPlayer:null,
    smallPlayAjaxObj:null,
    hasRequestSmallPlay:false,
    smallPlayJsonData:null,
    smallVideoData:null,
    msgTimeout:-1,
    num:0,

    init: function(left,top,width,height,_assetid){
        var self = this;
        if(typeof iPanel == "undefined"){

            this.android_video = this.androidCreateMediaView(left,top,width,height);
            this.android_video.src = _assetid;
            this.android_video.play();
            return this.android_video;
        }
        var navCheckFlag = iPanel.getGlobalVar("vodNavCheck");
        if(navCheckFlag != "true"){
            box_log("navCheckFlag!=true");
            return;
        }
        //第一次创建就是播放
        this.createMediaView(left,top,width,height);
        this.getSmallPlayParams(_assetid,function (url) {
            self.playSmallAV(url)
        });
    },
    androidCreateMediaView: function(left,top,width,height){
        //然而并没有什么用  video好像就是要用标签写
        var android_video_containner = document.createElement("div");
        var android_video = document.createElement("video");
        android_video_containner.style.position = "absolute";
        android_video_containner.style.left = left+"px";
        android_video_containner.style.top = top+"px";
        android_video_containner.style.width = width+"px";
        android_video_containner.style.height = height+"px";
        android_video.style.position = "absolute";
        android_video.style.left = 0+"px";
        android_video.style.top = 0+"px";
        android_video.width = width;
        android_video.height = height;
        android_video.autoplay = "true";
        android_video.playsinline = "true";
        android_video["x5-video-player-type"]="x5";
        android_video["preload"] = "metadata";
        android_video_containner.appendChild(android_video);
        document.body.appendChild(android_video_containner);
        return android_video;
    },
    //创建播放区域
    createMediaView: function (left,top,width,height) {

        if(typeof MediaPlayer == "undefined"){
            //报错 并且返回 不执行
            return ;
        }
        this.mediaPlayer = new MediaPlayer();
        var mediaPlayer = this.mediaPlayer;
        this.mediaID = mediaPlayer.getPlayerInstanceID();
        //绑定对象到上下文
        var flag = mediaPlayer.bindPlayerInstance(this.mediaID);
        this.createPlayerSuccess = flag==0 ? true:false;

        //iPanel.debug("specialReport createMediaPlayer mediaID=" + mediaID + "__flag=" + flag + "__createPlayerSuccess=" + createPlayerSuccess);
        if(this.createPlayerSuccess){
            //创建窗口
            var rect = new Rectangle(left,top,width,height);//left: 631px; top: 145px; width: 581px; height: 329px;
            //设置窗口区域
            var flag1 = mediaPlayer.setVideoDisplayArea(rect);
            //设置窗口模式
            var flag2 = mediaPlayer.setVideoDisplayMode(0);
            //刷新页面显示
            var flag3 = mediaPlayer.refresh();
            //iPanel.debug("specialReport createMediaPlayer flag1=" + flag1 + "__flag3=" + flag3+"__rect.left=="+rect.left+"__rect.top=="+rect.top+"__rect.width=="+rect.width+"__rect.height=="+rect.height);

        }else{
            iPanel.debug("specialReport createMediaPlayer mediaPlayer create failed");
        }
    },
    //通过媒资id获取播放地址
    getSmallPlayParams: function getSmallPlayParams(_assetid,callback){
        //判断是否请求过数据 有则返回
        if(this.hasRequestSmallPlay) return;
        //第一次请求
        this.hasRequestSmallPlay = true;
        if(typeof MediaPlayer == "undefined"){
            return;
        }
        // digestList 是Ipanel机顶盒的内置对象吧
        // iPanel.debug("getSmallPlayParams length=="+digestList.length+";; digestList[0]=="+ digestList[0].titleFull);
        //if(digestList.length == 0) return;

        //this.smallVideoData = digestList[num];
        //var smallVideoData = this.smallVideoData;
        // iPanel.debug("index getSmallPlayParams  digestList.length=== "+digestList.length);
        //开始请求
        if (this.smallPlayAjaxObj == null) {//第二次进就不需要再次加载 ajaxClass 类
            this.smallPlayAjaxObj = new ajaxClass();
            this.smallPlayAjaxObj.frame = window;
        } else {
            //请求终止 防止多次重复请求
            this.smallPlayAjaxObj.requestAbort();
        }
        //ajax成功回调
        this.smallPlayAjaxObj.successCallback = function(_xmlHttp, _params) {
            //iPanel.debug("specialReport getSmallPlayParams successCallback");
            var data = parseDom(this.getResponseXML());
            //转换JSON

            var jsonData = parseJson(data.toJson());
            if(typeof (jsonData)!= "object" || jsonData == null){
                //iPanel.debug("specialReport getSmallPlayParams error! error code: " + jsonData.error_code);
                return;
            }
            var key = "NavServerResponse";
            //iPanel.debug("specialReport getSmallPlayParams key in jsonData=" + (key in jsonData));
            // NavServerResponse 这个属性是否在json中
            if(key in jsonData){//是

                //错误
                var message = jsonData.NavServerResponse[0].message;
                var code = jsonData.NavServerResponse[0].code;
                box_log("specialReport getSmallPlayParams message="+message+"&code="+code)
                iPanel.debug("specialReport getSmallPlayParams message="+message+"&code="+code);
            }else{//否
                //请求成功  播放成功
                iPanel.debug("specialReport getSmallPlayParams  totalResult="+jsonData.Channels[0].totalResult);
                var rtsp = jsonData.StartResponse[0].rtsp.split(";");
                var serverId = rtsp[2].split(":");
                this.smallPlayUrl = rtsp[0] + ";" + rtsp[1] + ";" + serverId[0] + ":8080" + ";areacode=" + VOD.areaCode + ";client=" + CAManager.cardSerialNumber;
                iPanel.debug("index smallPlayUrl=="+smallPlayUrl);
                this.smallPlayJsonData = jsonData;
                callback(this.smallPlayUrl);
                //this.playSmallAV(this.smallPlayUrl);
            }
        };
        // 失败回调
        this.smallPlayAjaxObj.failureCallback = function(_xmlHttp, _params) {
            iPanel.debug("specialReport getSmallPlayParams request failed");
        };

        //密匙
        var account = this.account;
        //请求的参数
        this.smallPlayAjaxObj.urlParameters = "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><SelectionStart titleAssetId='"+_assetid+"' folderAssetId='"+_assetid+"' portalId='1' client='"+CAManager.cardSerialNumber + "' account='" + account + "' serviceId='100800000005'/>";
        //请求地址  获取url地址
        this.smallPlayAjaxObj.url = "http://" + this.ip + ":" + this.port +"/SelectionStart";
        //iPanel.debug("specialReport getSmallPlayParams urlParameters=" + smallPlayAjaxObj.urlParameters);
        //iPanel.debug("specialReport getSmallPlayParams url=" + smallPlayAjaxObj.url);
        // 发出请求
        this.smallPlayAjaxObj.requestData("POST");

    },
    playSmallAV:function (url){

        // var url = url ||'';  var url = url 这种写法是不行的  会不兼容高清盒子？？

        //iPanel.debug("specialReport playSmallAV smallPlayUrl=" + smallPlayUrl);
        if(url != ""){
            //VOD
            GlobalVarManager.setItemStr("playType","VOD");
            //设置正确的url
            this.mediaPlayer.setMediaSource(url);
            this.mediaPlayer.play();
        }
    },
    stopMedia: function stopMedia(){

        iPanel.debug("index__exitPage__createPlayerSuccess=" + this.createPlayerSuccess);
        if(this.smallPlayAjaxObj != null) {
            this.smallPlayAjaxObj.requestAbort();
        }

        // if(tipAjaxObj!=null){
        //     tipAjaxObj.requestAbort();
        // }

        if(this.createPlayerSuccess){
            this.mediaPlayer.stop();
            this.mediaPlayer.unbindPlayerInstance(this.mediaID);//：MediaPlayer对象和当前播放器实例解除绑定，并释放播放器的相关资源。
            //释放缓存
            this.mediaPlayer = null;
            this.mediaID = 0;
            this.createPlayerSuccess = false;
            this.smallPlayUrl = "";
            this.smallPlayAjaxObj = null;
            this.hasRequestSmallPlay = false;
            this.smallPlayJsonData = null;
            this.smallVideoData = null;
        }
    }
};




function navCheck(call){//认证

    if(typeof GlobalVarManager!="undefined"&&GlobalVarManager.getItemStr("vodNavCheck") == "true"){
        return "true";
    }

    var ip = "10.215.0.12";
    var port = "8080";
    var portalUrl = "http://portal.fjgdwl.com:8080/portal_new/getID.htm";  //portal地址
    var navCheckAjaxUrl = "http://" + ip + ":" + port + "/NavCheck";
    var navAjaxObj = null;
    var isNavChecking = false;					//是否正在执行认证
    var isAutoNavCheck = false;					//是否自动认证
    var navCheckTimeout = -1;					//认证过程16秒超时的计时器
    var navCheckCardId = "";					//认证用到的智能卡

    if(typeof GlobalVarManager !="undefined"){
        GlobalVarManager.setItemStr("tvPortalUrl",portalUrl);
        GlobalVarManager.setItemStr("ip",ip);
        GlobalVarManager.setItemStr("port",port);
    }


    if(typeof  Utility == "undefined"){
        return;
    }
    Utility.println("portal navCheck _flag="+_flag+" isNavChecking="+isNavChecking);
    // isAutoNavCheck = _flag == "auto" ? true : false;
    // if(isNavChecking) return;
    GlobalVarManager.setItemStr("vodNavCheck","false");
    if(navAjaxObj == null){
        navAjaxObj = new ajaxClass();
        navAjaxObj.frame = window;
    }else{
        navAjaxObj.requestAbort();
    }
    isNavChecking = true;
    navAjaxObj.successCallback = function(_xmlHttp, _params) {
        clearTimeout(navCheckTimeout);
        var data = parseDom(this.getResponseXML());
        var jsonData = parseJson(data.toJson());
        var key = "NavServerResponse";
        if(key in jsonData){
            var message = jsonData.NavServerResponse[0].message;
            var code = jsonData.NavServerResponse[0].code;
            Utility.println("portal navCheck success message="+message+" code="+code);
            isNavChecking = false;
            if(!isAutoNavCheck){
                showReminderTips(msg);
                $("loadingImg").style.visibility = "hidden";
            }
            return;
        }
        var account = jsonData.NavCheckResult[0].account;
        GlobalVarManager.setItemStr("account",account);
        var vodAjaxInfo = ip + "&" + port + "&" + account;
        var vodAjaxInfoObj = '{"vodAjaxInfo":"' + vodAjaxInfo+ '"}';
        setDataAccessProperty("info", "vodAjaxInfo", vodAjaxInfoObj, true);
        var frequency = jsonData.NavCheckResult[0].ZoneFreqInfo[0].frequency;
        Utility.println("portal navCheck frequency="+frequency);
        if(typeof frequency != "undefined" && frequency != "undefined" && frequency != ""){
            GlobalVarManager.setItemStr("frequency",frequency);
            var str = "IPQAMPointList=" + frequency + ",0;END";
            VOD.searchParams(str);
            clearTimeout(navCheckTimeout);
            navCheckTimeout = setTimeout(function (){
                isNavChecking = false;
                if(!isAutoNavCheck){
                }
            },16000);
        }
        else{
            isNavChecking = false;
            if(!isAutoNavCheck){

            }
        }
    };
    navAjaxObj.failureCallback = function (_xmlHttp, _params){
        Utility.println("portal navCheck failureCallback");
    };
    navCheckCardId = CAManager.cardSerialNumber;
    navAjaxObj.urlParameters = "<?xml version='1.0' encoding='UTF-8'?><NavCheck portalId='1' client='" + navCheckCardId + "' userType='0'/>";
    navAjaxObj.url = navCheckAjaxUrl;
    var header = {"Content-type":"text/xml;charset=utf-8"};
    navAjaxObj.requestData("POST",header);
    Utility.println("portal navCheck urlParameters="+navAjaxObj.urlParameters);
    Utility.println("portal navCheck url="+navAjaxObj.url);
    clearTimeout(navCheckTimeout);
    navCheckTimeout = setTimeout(function (){
        isNavChecking = false;
        if(!isAutoNavCheck){
        }
    },16000);
}


/****************************ajax请求 start**************************************/
function ajaxClass(_url, _successCallback, _failureCallback, _urlParameters, _callbackParams, _async, _charset, _timeout, _frequency, _requestTimes, _frame) {
    /**
     * AJAX通过GET或POST的方式进行异步或同步请求数据
     * 注意：
     * 	1、服务器240 No Content是被HTTP标准视为请求成功的
     * 	2、要使用responseXML就不能设置_charset，需要直接传入null
     * 	3、_frame，就是实例化本对象的页面对象，请尽量保证准确，避免出现难以解释的异常
     */
    /**
     * @param{string} _url: 请求路径
     * @param{function} _successCallback: 请求成功后执行的回调函数，带一个参数（可扩展一个）：new XMLHttpRequest()的返回值
     * @param{function} _failureCallback: 请求失败/超时后执行的回调函数，参数同成功回调，常用.status，.statusText
     * @param{string} _urlParameters: 请求路径所需要传递的url参数/数据
     * @param{*} _callbackParams: 请求结束时在回调函数中传入的参数，自定义内容
     * @param{boolean} _async: 是否异步调用，默认为true：异步，false：同步
     * @param{string} _charset: 请求返回的数据的编码格式，部分iPanel浏览器和IE6不支持，需要返回XML对象时不能使用
     * @param{number} _timeout: 每次发出请求后多长时间内没有成功返回数据视为请求失败而结束请求（超时）
     * @param{number} _frequency: 请求失败后隔多长时间重新请求一次
     * @param{number} _requestTimes: 请求失败后重新请求多少次
     * @param{object} _frame: 窗体对象，需要严格控制，否则会有可能出现页面已经被销毁，回调还执行的情况
     */
    this.url = _url || "";
    this.successCallback = _successCallback || function(_xmlHttp, _params) {
        iPanel.debug("[xmlHttp] responseText: " + _xmlHttp.responseText);
    };
    this.failureCallback = _failureCallback || function(_xmlHttp, _params) {
        iPanel.debug("[xmlHttp] status: " + _xmlHttp.status + ", statusText: " + _xmlHttp.statusText);
    };
    this.urlParameters = _urlParameters || "";
    this.callbackParams = _callbackParams || null;
    this.async = typeof(_async) == "undefined" ? true : _async;
    this.charset = _charset || null;
    this.timeout = _timeout || 5000; //20s
    this.frequency = _frequency || 500; //10s
    this.requestTimes = _requestTimes || 1;
    this.frame = _frame || window;

    this.timer = -1;
    this.counter = 0;

    this.method = "GET";
    this.headers = null;
    this.username = "";
    this.password = "";

    this.createXmlHttpRequest = function() {
        var xmlHttp = null;
        try { //Standard
            xmlHttp = new XMLHttpRequest();
        } catch (exception) { //Internet Explorer
            try {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (exception) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (exception) {
                    return false;
                }
            }
        }
        return xmlHttp;
    };
    this.xmlHttp = this.createXmlHttpRequest();

    this.requestData = function(_method, _headers, _username, _password) {
        /**
         * @param{string} _method: 传递数据的方式，POST/GET
         * @param{string} _headers: 传递数据的头信息，json格式
         * @param{string} _username: 服务器需要认证时的用户名
         * @param{string} _password: 服务器需要认证时的用户密码
         */
        this.frame.clearTimeout(this.timer);
        this.method = typeof(_method) == "undefined" ? "GET" : (_method.toLowerCase() == "post") ? "POST" : "GET";
        this.headers = typeof(_headers) == "undefined" ? null : _headers;
        this.username = typeof(_username) == "undefined" ? "" : _username;
        this.password = typeof(_password) == "undefined" ? "" : _password;
        iPanel.debug("[xmlHttp] method=" + this.method + "-->headers=" + _headers + "-->username=" +  this.username + "-->password=" + this.password);
        var target = this;
        var url;
        var data;
        this.xmlHttp.onreadystatechange = function() {
            target.stateChanged();
        };
        if (this.method == "POST") { //encodeURIComponent
            url = encodeURI(this.url);
            data = this.urlParameters;
        } else {
            url = encodeURI(this.url + (((this.urlParameters != "" && this.urlParameters.indexOf("?") == -1) && this.url.indexOf("?") == -1) ? ("?" + this.urlParameters) : this.urlParameters));
            data = null;
        }
        iPanel.debug("[xmlHttp] username=" + this.username + "-->xmlHttp=" + this.xmlHttp + "typeof(open)=" + typeof(this.xmlHttp.open));
        if (this.username != "") {
            this.xmlHttp.open(this.method, url, this.async, this.username, this.password);
        } else {
            this.xmlHttp.open(this.method, url, this.async);
        }
        iPanel.debug("[xmlHttp] method=" + this.method + "-->url=" + url + "-->async=" + this.async);
        var contentType = false;
        if (this.headers != null) {
            for (var key in this.headers) {
                if (key.toLowerCase() == "content-type") {
                    contentType = true;
                }
                iPanel.debug("common__contentType=" + contentType);
                this.xmlHttp.setRequestHeader(key, this.headers[key]);
            }
        }
        if (!contentType) {
            iPanel.debug("[xmlHttp] setRequestHeader");
            //this.xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            //this.xmlHttp.setRequestHeader("Content-type","text/xml;charset=utf-8");
            this.xmlHttp.setRequestHeader("Content-type","application/xml;charset=utf-8")
        }
        if (this.charset != null) { //要使用responseXML就不能设置此属性
            this.xmlHttp.overrideMimeType("text/html; charset=" + this.charset);
        }
        iPanel.debug("[xmlHttp] " + this.method + " url: " + url + ", data: " + data);
        this.xmlHttp.send(data);
    };
    this.stateChanged = function() { //状态处理
        if (this.xmlHttp.readyState < 2) {
            iPanel.debug("[xmlHttp] readyState=" + this.xmlHttp.readyState);
        } else {
            iPanel.debug("[xmlHttp] readyState=" + this.xmlHttp.readyState + ", status=" + this.xmlHttp.status);
        }

        var target = this;
        if (this.xmlHttp.readyState == 2) {
            this.timer = this.frame.setTimeout(function() {
                target.checkStatus();
            }, this.timeout);
        } else if (this.xmlHttp.readyState == 3) {
            if (this.xmlHttp.status == 401) {
                iPanel.debug("[xmlHttp] Authentication, need correct username and pasword");
            }
        } else if (this.xmlHttp.readyState == 4) {
            this.frame.clearTimeout(this.timer);
            if (this.xmlHttp.status == 200 || this.xmlHttp.status == 204) {
                this.success();
            } else {
                this.failed();
            }
        }
    };
    this.success = function() {
        if (this.callbackParams == null) {
            this.successCallback(this.xmlHttp);
        } else {
            this.successCallback(this.xmlHttp, this.callbackParams);
        }
        this.counter = 0;
    };
    this.failed = function() {
        if (this.callbackParams == null) {
            this.failureCallback(this.xmlHttp);
        } else {
            this.failureCallback(this.xmlHttp, this.callbackParams);
        }
        this.counter = 0;
    };
    this.checkStatus = function() { //超时处理，指定时间内没有成功返回信息按照失败处理
        if (this.xmlHttp.readyState != 4) {
            if (this.counter < this.requestTimes) {
                this.requestAgain();
            } else {
                iPanel.debug("[xmlHttp] readyState=" + this.xmlHttp.readyState + ", status=" + this.xmlHttp.status + " timeout");
                this.failed();
                this.requestAbort();
            }
        }
    };
    this.requestAgain = function() {
        this.requestAbort();
        var target = this;
        this.frame.clearTimeout(this.timer);
        this.timer = this.frame.setTimeout(function() {
            iPanel.debug("[xmlHttp] request again");
            target.counter++;
            target.requestData(target.method, target.headers, target.username, target.password);
        }, this.frequency);
    };
    this.requestAbort = function() {
        iPanel.debug("[xmlHttp] call abort");
        this.frame.clearTimeout(this.timer);
        this.xmlHttp.abort();
        iPanel.debug("[xmlHttp] call abort has called");
    };
    this.addParameter = function(_json) {
        /**
         * @param{object} _json: 传递的参数数据处理，只支持json格式
         */
        var url = this.url;
        var str = this.urlParameters;
        for (var key in _json) {
            if (url.indexOf("?") != -1) {
                url = "";
                if (str == "") {
                    str = "&" + key + "=" + _json[key];
                } else {
                    str += "&" + key + "=" + _json[key];
                }
                continue;
            }
            if (str == "") {
                str += "?";
            } else {
                str += "&";
            }
            str += key + "=" + _json[key];
        }
        this.urlParameters = str;
        return str;
    };
    this.getResponseXML = function() { //reponseXML of AJAX is null when response header 'Content-Type' is not include string 'xml', not like 'text/xml', 'application/xml' or 'application/xhtml+xml'
        if (this.xmlHttp.responseXML != null) {
            return this.xmlHttp.responseXML;
        } else if (this.xmlHttp.responseText.indexOf("<?xml") != -1) {
            return typeof(DOMParser) == "function" ? (new DOMParser()).parseFromString(this.xmlHttp.responseText, "text/xml") : (new ActivexObject("MSXML2.DOMDocument")).loadXML(this.xmlHttp.responseText);
        }
        return null;
    };
}
/****************************ajax请求 end**************************************/



//******************************解析XML文件 start**********************************//
function xmlDocToJson(_xmlDoc, _arrayNames, _attributes) {
    /**
     * xmlDocToJson 非 attributes 方法：存在多个相同名称子节点时需要指定父节点及子节点名称，子节点作为数组
     * xmlDocToJson attributes 方法：属性与子节点并存时，没有其他方案能够表现在JSON中，所以解决方案为给属性0赋值，访问子节点属性时要使用[0]
     * 解析XML格式情况有限，存在的问题有：
     *    1. 节点下有文本且有其他子节点时，会忽略文本
     *    2. 如果没有指定子节点是数组形式，没有子节点时，节点的值为字符串格式，而非对象
     *    3. 非 attributes 方法，子节点是数组形式，孙节点是数组形式，数组形式不能延续下去了，attributes 方法孙节点不能数组形式
     * xmlDocToJson(xmlDoc, [{"AppInfos": "AppInfo"}, {"AppRelatedPics": "appRelatedPic"}, {"AppGenres": "appGenreInfo"}]);
     * xmlDocToJson(xmlDoc, [{"AppInfos": "AppInfo"}, {"AppRelatedPics": "appRelatedPic"}, {"AppGenres": "appGenreInfo"}], true);
     */
    function xmlStrToXmlDoc(_xmlStr) {
        if (window.DOMParser) {
            return (new DOMParser()).parseFromString(_xmlStr, "text/xml");
        } else {
            var DOMParser = new ActiveXObject("Microsoft.XMLDOM");
            DOMParser.async = false;
            return DOMParser.loadXML(_xmlStr);
        }
    }
    if (typeof(_xmlDoc) == "string") {
        _xmlDoc = xmlStrToXmlDoc(_xmlDoc);
    }
    if (_xmlDoc == null) {
        iPanel.debug("[ERROR] the argument '_xmlDoc' is equal to null. (reponseXML of AJAX is null when response header 'Content-Type' is not include string 'xml', not like 'text/xml', 'application/xml' or 'application/xhtml+xml')");
        return {};
    }
    /*if (_arrayNames instanceof Array == false) { //instanceof Array test fails when using iframes
        if (typeof(_arrayNames) == "object" && _arrayNames != null) {
            _arrayNames = [_arrayNames];
        } else {
            _arrayNames = [];
        }
    }*/
    if (typeof(_arrayNames) == "undefined" || _arrayNames == null) {
        _arrayNames = [];
    }
    var obj = {};
    for (var i = 0; i < _xmlDoc.childNodes.length; i++) {
        var nodeName = ["", ""];
        out_1:
            for (var j = 0; j < _arrayNames.length; j++) {
                for (var key in _arrayNames[j]) {
                    if (key == _xmlDoc.childNodes[i].nodeName || (arguments.length > 3 && _arrayNames[j][key] == _xmlDoc.childNodes[i].nodeName)) {
                        nodeName = [_arrayNames[j][key], key];
                        obj[_xmlDoc.childNodes[i].nodeName] = {};
                        //_arrayNames.splice(j, 1);
                        break out_1;
                    }
                }
            }
        if (_attributes) {
            if (_xmlDoc.childNodes[i].nodeName == "#text") {
                continue;
            }
            if (nodeName[0] != "") {
                if (_xmlDoc.childNodes[i].nodeName == nodeName[1]) {
                    if (_xmlDoc.childNodes[i].childNodes.length == 0 || (_xmlDoc.childNodes[i].childNodes.length == 1 && _xmlDoc.childNodes[i].childNodes[0].nodeName == "#text")) {
                        out_5:
                            for (var j = 0; j < _arrayNames.length; j++) {
                                for (var key in _arrayNames[j]) {
                                    if (key == nodeName[1]) {
                                        if (typeof(obj[_xmlDoc.childNodes[i].nodeName]) == "undefined") {
                                            obj[_xmlDoc.childNodes[i].nodeName] = {};
                                        }
                                        obj[_xmlDoc.childNodes[i].nodeName][_arrayNames[j][key]] = [];
                                        break out_5;
                                    }
                                }
                            }
                    }
                }
                for (var j = 0; j < _xmlDoc.childNodes[i].childNodes.length; j++) {
                    if (_xmlDoc.childNodes[i].childNodes[j].nodeName == "#text") {
                        continue;
                    }
                    if (_xmlDoc.childNodes[i].childNodes[j].nodeName == nodeName[0]) {
                        if ((_xmlDoc.childNodes[i].childNodes[j].childNodes.length == 0 && _xmlDoc.childNodes[i].childNodes[j].nodeName != "#text") || (_xmlDoc.childNodes[i].childNodes[j].childNodes.length == 1 && _xmlDoc.childNodes[i].childNodes[j].childNodes[0].nodeName == "#text")) {
                            var attrObj = {};
                            for (var k = 0; k < _xmlDoc.childNodes[i].childNodes[j].attributes.length; k++) {
                                var attr = _xmlDoc.childNodes[i].childNodes[j].attributes[k];
                                attrObj[attr.name] = attr.value;
                            }
                            if (typeof(obj[_xmlDoc.childNodes[i].nodeName][nodeName[0]]) == "undefined") {
                                obj[_xmlDoc.childNodes[i].nodeName][nodeName[0]] = [];
                            }
                            obj[_xmlDoc.childNodes[i].nodeName][nodeName[0]].push(attrObj);
                        } else {
                            if (typeof(obj[_xmlDoc.childNodes[i].nodeName][0]) == "undefined") {
                                obj[_xmlDoc.childNodes[i].nodeName]["0"] = {};
                            }
                            if (typeof(obj[_xmlDoc.childNodes[i].nodeName]["0"][nodeName[0]]) == "undefined") {
                                obj[_xmlDoc.childNodes[i].nodeName]["0"][nodeName[0]] = {};
                            }
                            if (typeof(obj[_xmlDoc.childNodes[i].nodeName]["0"][nodeName[0]]["0"]) == "undefined") {
                                obj[_xmlDoc.childNodes[i].nodeName]["0"][nodeName[0]]["0"] = [];
                            }
                            obj[_xmlDoc.childNodes[i].nodeName]["0"][nodeName[0]]["0"].push(xmlDocToJson(_xmlDoc.childNodes[i].childNodes[j], _arrayNames, true));
                            for (var k = 0; k < _xmlDoc.childNodes[i].childNodes[j].attributes.length; k++) {
                                var attr = _xmlDoc.childNodes[i].childNodes[j].attributes[k];
                                obj[_xmlDoc.childNodes[i].nodeName]["0"][nodeName[0]][attr.name] = attr.value;
                            }
                        }
                    } else {
                        var arrFormat = false;
                        out_6:
                            for (var k = 0; k < _arrayNames.length; k++) {
                                for (var key in _arrayNames[k]) {
                                    if (key == nodeName[1] && _xmlDoc.childNodes[i].childNodes[j].nodeName == _arrayNames[k][key]) {
                                        arrFormat = true;
                                        break out_6;
                                    }
                                }
                            }
                        if (arrFormat == true) {
                            if (typeof(obj[_xmlDoc.childNodes[i].nodeName][_xmlDoc.childNodes[i].childNodes[j].nodeName]) == "undefined") {
                                obj[_xmlDoc.childNodes[i].nodeName][_xmlDoc.childNodes[i].childNodes[j].nodeName] = {};
                            }
                            if (typeof(obj[_xmlDoc.childNodes[i].nodeName][_xmlDoc.childNodes[i].childNodes[j].nodeName]["0"]) == "undefined") {
                                obj[_xmlDoc.childNodes[i].nodeName][_xmlDoc.childNodes[i].childNodes[j].nodeName]["0"] = [];
                            }
                            obj[_xmlDoc.childNodes[i].nodeName][_xmlDoc.childNodes[i].childNodes[j].nodeName]["0"].push(xmlDocToJson(_xmlDoc.childNodes[i].childNodes[j], _arrayNames, true));
                        } else {
                            if (typeof(obj[_xmlDoc.childNodes[i].nodeName][0]) == "undefined") {
                                for (var j = 0; j < _xmlDoc.childNodes[i].attributes.length; j++) {
                                    var attr = _xmlDoc.childNodes[i].attributes[j];
                                    obj[_xmlDoc.childNodes[i].nodeName][attr.name] = attr.value;
                                }
                                obj[_xmlDoc.childNodes[i].nodeName]["0"] = {};
                            }
                            if (_xmlDoc.childNodes[i].childNodes[j].nodeName == "#text") {
                                continue;
                            }
                            if (typeof(obj[_xmlDoc.childNodes[i].nodeName]["0"][_xmlDoc.childNodes[i].childNodes[j].nodeName]) == "undefined") {
                                obj[_xmlDoc.childNodes[i].nodeName]["0"][_xmlDoc.childNodes[i].childNodes[j].nodeName] = {};
                            }
                            obj[_xmlDoc.childNodes[i].nodeName]["0"][_xmlDoc.childNodes[i].childNodes[j].nodeName]["0"] = xmlDocToJson(_xmlDoc.childNodes[i].childNodes[j], _arrayNames, true);
                        }
                    }
                }
            } else {
                obj[_xmlDoc.childNodes[i].nodeName] = {};
                for (var j = 0; j < _xmlDoc.childNodes[i].attributes.length; j++) {
                    var attr = _xmlDoc.childNodes[i].attributes[j];
                    obj[_xmlDoc.childNodes[i].nodeName][attr.name] = attr.value;
                }
                obj[_xmlDoc.childNodes[i].nodeName]["0"] = xmlDocToJson(_xmlDoc.childNodes[i], _arrayNames, true);
            }
        } else {
            if (_xmlDoc.childNodes[i].childNodes.length == 0 && _xmlDoc.childNodes[i].nodeName != "#text") { //no character(space) between xml tags
                if (_xmlDoc.childNodes[i].nodeName == nodeName[1]) {
                    out_2:
                        for (var j = 0; j < _arrayNames.length; j++) {
                            for (var key in _arrayNames[j]) {
                                if (key == nodeName[1]) {
                                    if (typeof(obj[_xmlDoc.childNodes[i].nodeName]) == "undefined") {
                                        obj[_xmlDoc.childNodes[i].nodeName] = {};
                                    }
                                    obj[_xmlDoc.childNodes[i].nodeName][_arrayNames[j][key]] = [];
                                    break out_2;
                                }
                            }
                        }
                } else {
                    obj[_xmlDoc.childNodes[i].nodeName] = "";
                }
            } else if (_xmlDoc.childNodes[i].childNodes.length == 1 && _xmlDoc.childNodes[i].childNodes[0].nodeName == "#text") { //have character(s) between xml tags
                if (_xmlDoc.childNodes[i].nodeName == nodeName[1]) {
                    out_3:
                        for (var j = 0; j < _arrayNames.length; j++) {
                            for (var key in _arrayNames[j]) {
                                if (key == nodeName[1]) {
                                    if (typeof(obj[_xmlDoc.childNodes[i].nodeName]) == "undefined") {
                                        obj[_xmlDoc.childNodes[i].nodeName] = {};
                                    }
                                    obj[_xmlDoc.childNodes[i].nodeName][_arrayNames[j][key]] = [];
                                    break out_3;
                                }
                            }
                        }
                } else {
                    obj[_xmlDoc.childNodes[i].nodeName] = _xmlDoc.childNodes[i].childNodes[0].nodeValue;
                }
            } else {
                if (_xmlDoc.childNodes[i].nodeName == "#text") {
                    if (_xmlDoc.childNodes[i].childNodes.length == 0 && _xmlDoc.childNodes[i].parentNode.childNodes.length == 1) {
                        obj = _xmlDoc.childNodes[i].nodeValue;
                    }
                    continue;
                }
                if (nodeName[0] != "") {
                    for (var j = 0; j < _xmlDoc.childNodes[i].childNodes.length; j++) {
                        if (_xmlDoc.childNodes[i].childNodes[j].nodeName == "#text") {
                            continue;
                        }
                        if (_xmlDoc.childNodes[i].childNodes[j].nodeName == nodeName[0] || (arguments.length > 3 && _xmlDoc.childNodes[i].nodeName == nodeName[0])) {
                            if (arguments.length > 3) {
                                if (typeof(obj[_xmlDoc.childNodes[i].nodeName]) == "undefined" || obj[_xmlDoc.childNodes[i].nodeName] instanceof Array == false) {
                                    obj[_xmlDoc.childNodes[i].nodeName] = [];
                                }
                                obj[_xmlDoc.childNodes[i].nodeName].push(xmlDocToJson(_xmlDoc.childNodes[i], _arrayNames));
                                break;
                            }
                            if (typeof(obj[_xmlDoc.childNodes[i].nodeName][nodeName[0]]) == "undefined") {
                                obj[_xmlDoc.childNodes[i].nodeName][nodeName[0]] = [];
                            }
                            if (_xmlDoc.childNodes[i].childNodes[j].childNodes.length == 0 && _xmlDoc.childNodes[i].childNodes[j].nodeName != "#text") {
                                obj[_xmlDoc.childNodes[i].nodeName][nodeName[0]].push("");
                            } else if (_xmlDoc.childNodes[i].childNodes[j].childNodes.length == 1 && _xmlDoc.childNodes[i].childNodes[j].childNodes[0].nodeName == "#text") {
                                obj[_xmlDoc.childNodes[i].nodeName][nodeName[0]].push(_xmlDoc.childNodes[i].childNodes[j].childNodes[0].nodeValue);
                            } else {
                                obj[_xmlDoc.childNodes[i].nodeName][nodeName[0]].push(xmlDocToJson(_xmlDoc.childNodes[i].childNodes[j], _arrayNames, false, true));
                            }
                        } else {
                            var arrFormat = false;
                            out_4:
                                for (var k = 0; k < _arrayNames.length; k++) {
                                    for (var key in _arrayNames[k]) {
                                        if (key == nodeName[1] && _xmlDoc.childNodes[i].childNodes[j].nodeName == _arrayNames[k][key]) {
                                            arrFormat = true;
                                            break out_4;
                                        }
                                    }
                                }
                            if (arrFormat == true) {
                                if (typeof(obj[_xmlDoc.childNodes[i].nodeName][_xmlDoc.childNodes[i].childNodes[j].nodeName]) == "undefined") {
                                    obj[_xmlDoc.childNodes[i].nodeName][_xmlDoc.childNodes[i].childNodes[j].nodeName] = [];
                                }
                                obj[_xmlDoc.childNodes[i].nodeName][_xmlDoc.childNodes[i].childNodes[j].nodeName].push(xmlDocToJson(_xmlDoc.childNodes[i].childNodes[j], _arrayNames));
                            } else {
                                obj[_xmlDoc.childNodes[i].nodeName][_xmlDoc.childNodes[i].childNodes[j].nodeName] = xmlDocToJson(_xmlDoc.childNodes[i].childNodes[j], _arrayNames);
                            }
                        }
                    }
                } else {
                    obj[_xmlDoc.childNodes[i].nodeName] = xmlDocToJson(_xmlDoc.childNodes[i], _arrayNames);
                }
            }
        }
    }
    return obj;
}
//*******************************解析XML文件 end****************************//


//***************************************hashMap  start*******************************//
function hashTableClass(_maxLength) {
    /**
     * 仿写java中的hashmap对象，在eventFrame里进行数据缓存
     * @_maxLength：整型，缓存条目数量
     * 注意：
     * 	1、自行判定条目是否已存在，控制是否覆盖数据（也可在使用put方法时传入第三个布尔参数进行控制），否则将视为更新已存在的数据
     * 	2、delete系统方法，可能不被一些vane版本中间件支持
     */
    this.maxLength = typeof(_maxLength) == "undefined" ? 50 : _maxLength;

    this.hash = new Object();
    this.arry = new Array(); //记录条目的key

    this.put = function(_key, _value, _notCover) {
        /**
         * @_key：字符串型
         * @_value：不限制类型
         * @_notCover：布尔型，设定为真后不进行覆盖
         */
        if (typeof(_key) == "undefined") {
            return false;
        }
        if (this.contains(_key) == true) {
            if (_notCover) {
                return false;
            }
        }
        this.limit();
        if (this.contains(_key) == false) {
            this.arry.push(_key);
        }
        this.hash[_key] = typeof(_value) == "undefined" ? null : _value;
        return true;
    };
    this.get = function(_key) {
        if (typeof(_key) != "undefined") {
            if (this.contains(_key) == true) {
                return this.hash[_key];
            } else {
                return false;
            }
        } else {
            return false;
        }
    };
    this.remove = function(_key) {
        if (this.contains(_key) == true) {
            delete this.hash[_key];
            for (var i = 0, len = this.arry.length; i < len; i++) {
                if (this.arry[i] == _key) {
                    this.arry.splice(i, 1);
                    break;
                }
            }
            return true;
        } else {
            return false;
        }
    };
    //this.count = function() {var i = 0; for(var key in this.hash) {i++;} return i;};
    this.contains = function(_key) {
        return typeof(this.hash[_key]) != "undefined";
    };
    this.clear = function() {
        this.hash = {};
        this.arry = [];
    };
    this.limit = function() {
        if (this.arry.length >= this.maxLength) { //保存的对象数大于规定的最大数量
            var key = this.arry.shift(); //删除数组第一个数据，并返回数组原来第一个元素的值
            this.remove(key);
        }
    };
}
var hashTableObj = new hashTableClass(120);

/***************************************hashMap  end*******************************/


/**********************************将xml对象转化为json对象 start***********************/

/*
** Dom解析函数
** 适用xml文件和dom文档
** @frag:dom对象, xml文件数据
** @return: 返回一个可直接被引用的数据对象
*/
function parseDom(frag) {
    var obj = new Object;
    var childs = getChilds(frag);
    var len = childs.length;
    var attrs = frag.attributes;
    if(attrs != null) {
        for(var i = 0; i < attrs.length; i++) {
            iPanel.debug("common__parseDom__nodeName=" + attrs[i].nodeName + "__nodeValue=" + attrs[i].nodeValue);
            obj[attrs[i].nodeName] = attrs[i].nodeValue;
        }
    }
    if(len == 0){
        return obj;
    }
    else
    {
        var tags = new Array();
        for(var i = 0; i < len; i++) {
            if(!inArray(childs[i].nodeName, tags)) tags.push(childs[i].nodeName);
        }

        for(var i = 0; i < tags.length; i++) {
            var nodes = getChildByTag(tags[i]);
            obj[tags[i]] = new Array;
            for(var j = 0; j < nodes.length; j++) {
                obj[tags[i]].push(getValue(nodes[j]));
            }
        }
    }
    return obj;

    //判断是否存在于数组的私有方法
    function inArray(a, arr) {
        for(var i = 0; i < arr.length; i++) {
            if(arr[i] == a) return true;
        }
        return false;
    }

    /*
    * nodeType:1(元素element),2:(属性attr),3:(文本text),8:(注释comments),9:(文档documents)
    */
    //获取非文本类型子节点
    function getChilds(node) {
        var c = node.childNodes;// 返回包含被选节点的子节点的NodeList,如果选定的节点没有子节点，则该属性返回不包含节点的 NodeList。
        var a = new Array;
        if(c != null) {
            for(var i = 0; i < c.length; i++) {
                if(c[i].nodeType != 3) a.push(c[i]);
            }
        }
        return a;
    }

    //子元素中根据节点名来获取元素集合
    function getChildByTag(tag) {
        var a = new Array;
        for(var i = 0; i < len; i++) {
            if(childs[i].nodeName == tag) a.push(childs[i]);
        }
        return a;
    }

    //获取节点的文本，如果存在子节点则递归
    function getValue(node) {
        var c = getChilds(node);
        var obj_arr = new Object;
        if(c.length == 0) {
            if(node.firstChild){//文本
                obj_arr.value = node.firstChild.nodeValue;
            }
            var attrs = node.attributes;
            if(attrs != null){
                for(var i = 0; i < attrs.length; i++) {
                    obj_arr[attrs[i].nodeName] = attrs[i].nodeValue;
                }
            }
            return obj_arr;
        }
        else return parseDom(node);
    }
}

function parseJson(str) {
    //iPanel.debug("parse.js_parseJson_str="+str);
    eval('var val = ' + str + ';');
    return val;
}

//在数组原型上添加一个数组转json字符串的方法，需要对象转json方法
Array.prototype.toJson = function () {
    var arr = new Array;
    for(var i = 0; i < this.length; i++) {
        switch(typeof this[i]) {
            case 'number':
                arr[i] = this[i];
                break;
            case 'boolean':
                arr[i] = this[i];
                break;
            case 'string':
                arr[i] = '"' + this[i].replace(/"/g, '\\\"') + '"';
                break;
            case 'object':
                arr[i] = this[i].toJson();
                break;
        }
    }
    return '[' + arr.join(', ') + ']';
};

//在对象原型上添加一个数组转json字符串的方法，需要数组转json方法
Object.prototype.toJson = function () {
    if(typeof this == 'object') {
        if(this instanceof Array) {
            return this.toJson();
        } else {
            var arr = new Array;
            var str = '';
            for(var p in this) {
                if(typeof this[p] == 'function') break;
                switch(typeof this[p]) {
                    case 'number':
                        str = this[p];
                        break;
                    case 'boolean':
                        str = this[p];
                        break;
                    case 'string':
                        str = '"' + this[p].replace(/"/g, '\\\"') + '"';
                        break;
                    case 'object':
                        str = this[p].toJson();
                        break;
                }
                arr.push(p + ':' + str);
            }
            return '{' + arr.join(', ') + '}';
        }
    } else return 'not object';
};

/*****************************************将xml对象转换为json对象 end****************************/
/********************************showList***********************************/



//电视直播



var mediaPlayer;
var mediaID;
var createPlayerSuccess;
/*首页版块小视屏相关  start*/
function createMediaPlayer(left,top,width,height){//创建播放器对象
    if(typeof MediaPlayer == "undefined"){
        return;
    };
    mediaPlayer = new MediaPlayer();
    mediaID = mediaPlayer.getPlayerInstanceID();
    var flag = mediaPlayer.bindPlayerInstance(mediaID);
    createPlayerSuccess = flag == 0 ? true : false;
    if(createPlayerSuccess){
        var rect = new Rectangle(left,top,width,height);
        var flag1 = mediaPlayer.setVideoDisplayArea(rect);
        var flag2 = mediaPlayer.setVideoDisplayMode(0)
        var flag3 = mediaPlayer.refresh();
        GlobalVarManager.setItemStr("playType","DVB");
        GlobalVarManager.setItemStr("stopMode","0");
    }
}

function stopSmallAV(){//停止播放小视屏
    if(createPlayerSuccess){
        mediaPlayer.stop();
    }
}

function playSmallAV(){//播放小视屏，第一次进入则播放0频道，否则的话播放关机频道
    if(!createPlayerSuccess) return;
    var filterArray = [1000,1000];
    var valueArray = [1,2];
    var channelList = ChannelManager.filter(filterArray,valueArray);

    if(channelList.length == 0){
        //$("AVTips").innerText = "无节目，请重新搜索";
        //$("AVTips").style.visibility = "visible";
        return;
    }
    var currChannel = null;

    if(firstEnterIndex != "false"){
        var initPlayServiceId = GlobalVarManager.getItemStr("initPlayServiceId");

        var servcieId;
        for(var i = 0; i < channelList.length; i++){
            servcieId = channelList[i].getService().service_id;
            if(servcieId == initPlayServiceId){
                currChannel = channelList[i];
                break;
            }
        }

        if(currChannel){
            var location = currChannel.getService().getLocation();
            mediaPlayer.setMediaSource(location);//0频道不让编辑的，因此不用判断是否锁定
            return ;
        }
    }

    currChannel = ChannelManager.getShutDownChannel(128);

    if(!currChannel) currChannel = channelList[0];

    if(currChannel.type == 0x02 ){//广播频道
        //$("musicImg").src = "audio_bg.jpg";
        //$("musicBg").style.visibility = "visible";
    }
    else{
        //$("musicBg").style.visibility = "hidden";
    }
    var tableID = DataAccess.getUserPropertyTable("userInfo");
    var passwordEnable = DataAccess.getProperty(tableID, "passwordEnable");
    if(passwordEnable && currChannel.isLocked){//锁定的
        mediaPlayer.stop();
        //$("AVTips").style.visibility = "频道锁定";
        //$("AVTips").style.visibility = "visible";
    }
    else{
        var location = currChannel.getService().getLocation();
        mediaPlayer.setMediaSource(location);
    }
}