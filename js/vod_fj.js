
/*===========================获取列表数据start==========================================*/
var server_url = "http://192.168.11.41:8080";

//raw = "<" + "?xml version='1.0' encoding='UTF-8' standalone='yes'?><GetFolderContents assetId='" + this.searchInfo.labelId + "' portalId='1' client='" + this.getCAId() + "' account='" + this.getAccount() + "' startAt='" + (tmpSearchPage * this.programNum + 1) + "' maxItems='" + this.programNum + "' languageCode='Zh-CN' regionCode='1' includeFolderProperties='Y' includeSubFolder='Y' includeSelectableItem='Y' profile='1.0'/>";
/*-----------------start请求样例：
请求地址：
http://192.168.11.41:8080/GetFolderContents?dataType=json
请求报文：
<?xml version='1.0' encoding='UTF-8' standalone='yes'?><GetFolderContents assetId='MANU0000000000129781' portalId='1' client='8592003073772577' account='8592003073772577' startAt='1' maxItems='1' languageCode='Zh-CN' regionCode='1' includeFolderProperties='Y' includeSubFolder='Y' includeSelectableItem='Y' profile='1.0'/>

返回数据：
{
    "adsURL":"http://172.18.118.20:8080/delivery/",
    "carouselURL":"deliver://490000000:6875:64:1901",
    "childFolderList":[
		//luobr：子栏目或媒资包（电视剧）列表
		
    ],
    "currentResults":1,
    "folderFrame":{//栏目信息
        "assetId":"MANU0000000000129781",
        "chargeMode":"-2",
        "displayName":"聊斋夜话",
        "folderDetails":null,
        "folderName":"",
        "folderOrderType":"0",
        "folderShowType":"",
        "folderType":"1",
        "imageList":[
            {
                "height":246,
                "posterUrl":"poster_root/178x246/20151112/20151112113318067_0189.jpg",
                "rank":1,
                "width":178
            }
        ],
        "linkUrl":"/iPG/T-nsp/yytc/template/null/null",
        "parentAssetId":"MANU0000000000113826",//luobr：父栏目ID
        "providerId":"",
        "rentaInfo":{
            "expireDateTime":"",
            "markResumeDate":"",
            "orderFlag":"1",
            "purchaseToken":"",
            "rentDateTime":"",
            "resumePoint":0
        },
        "resourceId":"",
        "serviceCode":"vodxqfy_lzyh",
        "serviceId":""
    },
    "operatorCode":"100000",
    "orderFields":[

    ],
    "recommendItemList":[

    ],
    "restartAtToken":"2",
      "selectableItemList":[//子集或电影列表
        {
            "actorsDisplay":"",
            "assetId":"FFMZ0000000000025597",//播放的媒资ID，到这里就可以直接调用getDetailData(assetid);
            "assetType":"/福建广电/福州分公司/热门精选/聊斋夜话",
            "bookmarkDate":"",
            "chapter":1,
            "chargeMode":"-2",
            "detailURI":"",
            "director":[

            ],
            "displayFlags":"",
            "displayRunTime":"1748s",
            "endDateTime":"",
            "favorRating":"0",
            "flagImageUrl":"",
            "folderAssetId":"MANU0000000000129781",
            "folderDetails":null,
            "folderName":"聊斋夜话",
            "imageList":[

            ],
            "imageLocation":"",
            "initialLetter":"",
            "initialLetterToNumber":"",
            "isPackage":"0",
            "orderNumber":809,
            "originName":"",
            "previewAssetId":"FFMZ0000000000025597",
            "previewProviderId":"",
            "prividerName":"",
            "producter":[

            ],
            "providerId":"FZFGS",
            "publishDate":"2018-02-06",
            "rating":null,
            "recommandRating":0,
            "recommandTimes":0,
            "recommendationLevel":6,
            "rentaInfo":null,
            "resourceId":"",
            "resourceType":1,
            "runtime":"00:29:08",
            "secondTitleFull":"",
            "selectionChoice":[
                {
                    "audioType":"",
                    "displayPrice":"",
                    "format":"HD",
                    "languageSet":null,
                    "rentalPeriod":29,
                    "screenShape":"",
                    "viewLimit":0
                }
            ],
            "serviceId":"",
            "serviceType":"VOD",
            "showType":"0",
            "startDateTime":"",
            "status":"1",
            "subscription":null,
            "summarMedium":"聊斋夜话_20180205",
            "summarvShort":"聊斋夜话_20180205",
            "titleBrief":"聊斋夜话_20180205",
            "titleFull":"聊斋夜话_20180205",
            "videoType":"1",
            "viewLevel":"0",
            "year":2018
        },
        {
            "actorsDisplay":"",
            "assetId":"FFMZ0000000000025577",
            "assetType":"/福建广电/福州分公司/热门精选/聊斋夜话",
            "bookmarkDate":"",
            "chapter":1,
            "chargeMode":"-2",
            "detailURI":"",
            "director":[

            ],
            "displayFlags":"",
            "displayRunTime":"1738s",
            "endDateTime":"",
            "favorRating":"0",
            "flagImageUrl":"",
            "folderAssetId":"MANU0000000000129781",
            "folderDetails":null,
            "folderName":"聊斋夜话",
            "imageList":[

            ],
            "imageLocation":"",
            "initialLetter":"",
            "initialLetterToNumber":"",
            "isPackage":"0",
            "orderNumber":808,
            "originName":"",
            "previewAssetId":"FFMZ0000000000025577",
            "previewProviderId":"",
            "prividerName":"",
            "producter":[

            ],
            "providerId":"FZFGS",
            "publishDate":"2018-02-05",
            "rating":null,
            "recommandRating":0,
            "recommandTimes":0,
            "recommendationLevel":6,
            "rentaInfo":null,
            "resourceId":"",
            "resourceType":1,
            "runtime":"00:28:58",
            "secondTitleFull":"",
            "selectionChoice":[
                {
                    "audioType":"",
                    "displayPrice":"",
                    "format":"HD",
                    "languageSet":null,
                    "rentalPeriod":28,
                    "screenShape":"",
                    "viewLimit":0
                }
            ],
            "serviceId":"",
            "serviceType":"VOD",
            "showType":"0",
            "startDateTime":"",
            "status":"1",
            "subscription":null,
            "summarMedium":"聊斋爱读书_20180204",
            "summarvShort":"聊斋爱读书_20180204",
            "titleBrief":"聊斋爱读书_20180204",
            "titleFull":"聊斋爱读书_20180204",
            "videoType":"1",
            "viewLevel":"0",
            "year":2018
        }
    ],
    "specialCode":"4",
    "totalResults":150,
    "vodMaxPauseTime":"300"
}

--------------end请求样例*/

var test_raw = "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><GetFolderContents assetId='MANU0000000000308771' portalId='1' client='8592003073772577' account='8592003073772577' startAt='1' maxItems='200' languageCode='Zh-CN' regionCode='1' includeFolderProperties='Y' includeSubFolder='Y' includeSelectableItem='Y' profile='1.0'/>"

function GetFolderContents(_assetId,call,_startAt,_maxItems){
	//var idCard = getCAcardNum();
	var _startAt = Number(_startAt)||1;
    var _maxItems = Number(_maxItems)||10;
	luobrajax({
		method: 'POST',
		url: 'http://192.168.11.41:8080/GetFolderContents?dataType=json',//http://10.215.0.12:8080/
		data:"<?xml version='1.0' encoding='UTF-8' standalone='yes'?><GetFolderContents assetId='"+_assetId+"' portalId='1' client='8592003073772577' account='8592003073772577' startAt='"+_startAt+"' maxItems='"+_maxItems+"' languageCode='Zh-CN' regionCode='1' includeFolderProperties='Y' includeSubFolder='Y' includeSelectableItem='Y' profile='1.0'/>",
		//自定义属性，assetId：栏目id ； client&account：机顶盒CA卡号 ； startAt：从第几条开始请求，第一条就穿1 ； maxItems：每次请求数量
		success: function (response) {
			var json = eval('('+response+')');

            call(json);
		}
	});
}
function getCAcardNum(){
    var idCard="";
    if(typeof(CAManager) != "undefined"){
        idCard = CAManager.cardSerialNumber;
    } else if(typeof(hardware) != "undefined"){
        idCard = hardware.smartCard.serialNumber;
    }
    return idCard;
}

/*===========================获取列表数据end==========================================*/

/*===========================播放相关start==========================================*/
if(typeof GlobalVarManager != "undefined"){
    var account = GlobalVarManager.getItemStr("account");
    var playIp = "http://vod.fjgdwl.com:80/gldb/NEW_UI/vodPlay/";//去vod播放的路径
    var palyBackUrl = "";//播放返回的链接
    var movieObj;
    var area_code = VOD.areaCode;
    if(area_code == ""){
        area_code = VOD.server.regionId;
    }
    if(area_code == ""){
        area_code = VOD.server.nodeGroupID;
    };
}

function checkSavedProgram(assetId,_type){
	var idCard = getCAcardNum();
	luobrajax({
		method: 'POST',
		url: 'http://10.215.0.12:8080/CheckSavedProgram?dataType=json',
		data:"<?xml version='1.0' encoding='UTF-8' standalone='yes'?><CheckSavedProgram assetId='" + assetId + "' portalId='1' client='"+idCard+"' account='"+account+"' folderAssetId='" + assetId + "' profile='1.0' languageCode='zh-CN'/>",
		success: function (response) {
			Utility.println("luobr++ CheckSavedProgram====" + response);
			var json = eval('('+response+')');
			if(_type){//请求断点
				if(json.bookmarkFlag == "true"){
					getResumeParams(assetId,json.timePosition);
				}else{
					getPlayParams(assetId);
				}
			}else{//保存播放记录
				
			}
			
		}
	});
}

//获取详情为了断点播放用，恶心的同洲接口
function getDetailData(_assetId){
	luobrajax({
		method: 'POST',
		url: 'http://10.215.0.12:8080/GetItemData?dataType=json',
		data:"<?xml version='1.0' encoding='UTF-8' standalone='yes'?><GetItemData titleAssetId='" + _assetId + "' portalId='1' client='" + CAManager.cardSerialNumber + "' account='" + account + "' languageCode='Zh-CN' profile='1'/>",
		success: function (response) {
			Utility.println("luobr++ details====" + response);
			var json = eval('('+response+')');
			movieObj = json.selectableItem;
			Utility.println("luobr++ movieObj====" + movieObj.previewAssetId);
			checkSavedProgram(_assetId,1);
		}
	});
}
//从断点播放
function getResumeParams(_assetId,_timePosition){
	var idCard = getCAcardNum();
	luobrajax({
		method: 'POST',
		url: 'http://10.215.0.12:8080/SelectionResume?dataType=json',
		data:"<?xml version='1.0' encoding='UTF-8' standalone='yes'?><SelectionResume portalId='1'  client='" + idCard + "' account='" + account + "' titleAssetId='" + _assetId + "' purchaseToken='" + _timePosition +"' titleProviderId='" + movieObj.providerId + "' fromStart='N' />",
		success: function (response) {
			Utility.println("luobr++ SelectionResume====" + response);
			var json = eval('('+response+')');
			
			if (typeof(json) != "object" || json == null) {
				popUpObj.init("未知错误！");
				return;
			}

			if ("message" in json || "code" in json) {

				var message = json.message;
				var code = json.code;
				iDebug("[portal] index.js_getPlayParam_error_code=" + code);
				if (code == 13000001 || code == 12011073) { // 未授权
					var url = Q.getNewAddUrl("order.htm") + "backUrl=" + encodeURIComponent(palyBackUrl);
					Q.setGlobalVar("txzq_back_url", url);
					location.href = url;
				} else { // 其它错误
					popUpObj.init(message);
				}
				return;
			}
			
			//{"purchaseToken":"1809885953","rtsp":"rtsp://10.215.0.50:554/;purchaseToken=1809885953;serverID=10.215.0.12:8080"}
			var rtsp = json.rtsp.split(";");
			var serverId = rtsp[2].split(":");
			var playUrl = rtsp[0] + ";" + rtsp[1] + ";" + serverId[0] + ":8080" + ";areacode=" + area_code + ";client=" + CAManager.cardSerialNumber;
			
			if(1) {
				GlobalVarManager.setItemStr("vodPlayUrl",playUrl);
				window.location.href = playIp + "vodPlay.htm?previewId=" + json.previewAssetId + "&startTime=" + json.startTime + "&purchaseToken=" + json.purchaseToken + "&playCurrName=" + movieObj.titleFull + "&assetId=" + _assetId +  "&providerId=" + movieObj.providerId + "&displayRunTime=" + movieObj.displayRunTime + "&folderAssetId=" + _assetId  + "&resumePoint=" + _timePosition +"&from="+ encodeURIComponent(palyBackUrl);
			} else {
				var totalResult = selectionsObj.len;
				var chapter = selectionsObj.pos + selectionsObj.page_pos * selectionsObj.max_size + 1;
				GlobalVarManager.setItemStr("boundPlayUrl", playUrl);
				window.location.href = playIp + "boundPlay.htm?previewId=" + json.previewAssetId + "&startTime=" + json.startTime + "&purchaseToken=" + json.purchaseToken + "&playCurrName=" + movieObj.titleFull + "&assetId=" + movieObj.assetId + "&childAssetId=" + movieObj.assetId + "&providerId=" + movieObj.providerId + "&displayRunTime=" + movieObj.displayRunTime + "&folderAssetId=" + folderAssetId_this + "&resumePoint=" + _timePosition + "&chapter=" + chapter + "&totalResult=" + totalResult + "&autoPlayFlag=1&from=" + encodeURIComponent(palyBackUrl);
			}
		}
	});
}
//重头播放


function getPlayParams(_assetId){
	luobrajax({
		method: 'POST',
		url: 'http://10.215.0.12:8080/SelectionStart?dataType=json',
		data:"<?xml version='1.0' encoding='UTF-8' standalone='yes'?><SelectionStart titleAssetId='"+ _assetId +"' folderAssetId='"+ _assetId +"' portalId='1'  client='"+ CAManager.cardSerialNumber +"' account='"+ account +"'/>",
		success: function (response) {
			Utility.println("luobr++ SelectionResume====" + response);
			var json = eval('('+response+')');
			
			if (typeof(json) != "object" || json == null) {
				popUpObj.init("未知错误！");
				return;
			}
			if ("message" in json || "code" in json) {

				var message = json.message;
				var code = json.code;
				iDebug("[portal] index.js_getPlayParam_error_code=" + code);
				if (code == 13000001 || code == 12011073) { // 未授权
					var url = Q.getNewAddUrl("order.htm") + "backUrl=" + encodeURIComponent(palyBackUrl);
					Q.setGlobalVar("txzq_back_url", url);
					location.href = url;
				} else { // 其它错误
					popUpObj.init(message);
				}
				return;
			}
			
			//{"purchaseToken":"1809885953","rtsp":"rtsp://10.215.0.50:554/;purchaseToken=1809885953;serverID=10.215.0.12:8080"}
			var rtsp = json.rtsp.split(";");
			var serverId = rtsp[2].split(":");
			var playUrl = rtsp[0] + ";" + rtsp[1] + ";" + serverId[0] + ":8080" + ";areacode=" + area_code + ";client=" + CAManager.cardSerialNumber;
			
			if(1) {
				GlobalVarManager.setItemStr("vodPlayUrl",playUrl);
				window.location.href = playIp + "vodPlay.htm?previewId=" + json.previewAssetId + "&startTime=" + json.startTime + "&purchaseToken=" + json.purchaseToken + "&playCurrName=" + movieObj.titleFull + "&assetId=" + _assetId + "&providerId=" + movieObj.providerId + "&displayRunTime=" + movieObj.displayRunTime + "&folderAssetId=" + movieObj.folderAssetId + "&resumePoint=0&collectCI=" + movieObj.folderAssetId + "&collectCN=" + movieObj.titlebrief + "&from="+ encodeURIComponent(palyBackUrl);
			} else {
				//collectCN = collectAppID + "/" + collectSubID + ".1" + "/" + "A01/" + tempData.assetId;
				//其中folderAssetId为媒资包的ID
				//chapter当前要去播放的子集集数
				//totalResult总集数
				//autoPlayFlag=1允许用户设置连播
				//collectCI和collectCN作为收视率采集用，可不传
				var resumePoint = 0;//断点时间
				var totalResult = selectionsObj.len;
				var chapter = selectionsObj.pos + selectionsObj.page_pos * selectionsObj.max_size+ 1;
				GlobalVarManager.setItemStr("boundPlayUrl", playUrl);
				window.location.href = playIp + "boundPlay.htm?previewId=" + json.previewAssetId + "&startTime=" + json.startTime + "&purchaseToken=" + json.purchaseToken + "&playCurrName=" + movieObj.titleFull + "&assetId=" + movieObj.assetId + "&childAssetId=" + movieObj.assetId + "&providerId=" + movieObj.providerId + "&displayRunTime=" + movieObj.displayRunTime + "&folderAssetId=" + folderAssetId_this + "&resumePoint=" + resumePoint + "&chapter=" + chapter + "&totalResult=" + totalResult + "&autoPlayFlag=1&collectCI=" + movieObj.titlebrief + "&collectCN=" + movieObj.titlebrief + "&from=" + encodeURIComponent(palyBackUrl);
			}
		}
	});
}

function luobrajax(opt) {
	opt = opt || {};
	opt.method = opt.method.toUpperCase() || 'POST';
	opt.url = opt.url || '';
	opt.async = opt.async || true;
	opt.data = opt.data || null;
	opt.success = opt.success || function () {};
	var xmlHttp = null;
	if (XMLHttpRequest) {
		xmlHttp = new XMLHttpRequest();
	}
	else {
		xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
	}var params = [];
	for (var key in opt.data){
		params.push(key + '=' + opt.data[key]);
	}
	var postData = params.join('&');
	if (opt.method.toUpperCase() === 'POST') {
		xmlHttp.open(opt.method, opt.url, opt.async);
		xmlHttp.setRequestHeader('Content-Type', 'application/xml;charset=utf-8;Access-Control-Allow-Origin');
		xmlHttp.send(opt.data);
	}
	else if (opt.method.toUpperCase() === 'GET') {
		xmlHttp.open(opt.method, opt.url + '?' + postData, opt.async);
		xmlHttp.send(null);
	} 
	xmlHttp.onreadystatechange = function () {
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
			opt.success(xmlHttp.responseText);
		}
	};
}
/*===========================播放相关end==========================================*/