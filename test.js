const mergeSort = function(arr) {
    const len = arr.length;
    if (len > 1) {
        // 对半分解
        const middle = Math.floor(len / 2);
        const left = arr.slice(0, middle);
        const right = arr.slice(middle, len);
        let i = 0;
        let j = 0;
        let k = 0;
        // 分别对左右进行排序
        mergeSort(left);
        mergeSort(right);
        while(i < left.length && j < right.length) {
            if (left[i] < right[j]) {
                arr[k] = left[i];
                i++;
            } else {
                arr[k] = right[j];
                j++;
            }
            k++;
        }
        // 检查余项
        while(i < left.length) {
            arr[k] = left[i];
            i++;
            k++;
        }
        while(j < right.length) {
            arr[k] = right[j];
            j++;
            k++;
        }
    }
    return arr;
}
console.log(mergeSort([1,2,4,3]));